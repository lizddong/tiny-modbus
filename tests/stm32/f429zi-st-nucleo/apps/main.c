/**
  ******************************************************************************
  * @file    Templates/Src/main.c 
  * @author  MCD Application Team
  * @brief   Main program body
  *
  * @note    modified by ARM
  *          The modifications allow to use this file as User Code Template
  *          within the Device Family Pack.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2017 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "modbus_api.h"
#include "mbport_serial.h"
#include "mbport_network.h"

#ifdef _RTE_
#include "RTE_Components.h"             // Component selection
#endif
#ifdef RTE_CMSIS_RTOS2                  // when RTE component CMSIS RTOS2 is used
#include "cmsis_os2.h"                  // ::CMSIS:RTOS2
#endif

#ifdef RTE_CMSIS_RTOS2_RTX5
/**
  * Override default HAL_GetTick function
  */
uint32_t HAL_GetTick (void) {
  static uint32_t ticks = 0U;
         uint32_t i;

  if (osKernelGetState () == osKernelRunning) {
    return ((uint32_t)osKernelGetTickCount ());
  }

  /* If Kernel is not running wait approximately 1 ms then increment 
     and return auxiliary tick counter value */
  for (i = (SystemCoreClock >> 14U); i > 0U; i--) {
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
    __NOP(); __NOP(); __NOP(); __NOP(); __NOP(); __NOP();
  }
  return ++ticks;
}
#endif

/** @addtogroup STM32F4xx_HAL_Examples
  * @{
  */

/** @addtogroup Templates
  * @{
  */
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define MB_TAB_BITS_SIZES           (10)
#define MB_TAB_INPUT_BITS_SIZES     (10)
#define MB_TAB_INPUT_REGS_SIZES     (10)
#define MB_TAB_REGS_SIZES           (10)
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Modbus registers */
static mb_uint8_t tab_bits_array[MB_TAB_BITS_SIZES];
static mb_uint8_t tab_input_bits_array[MB_TAB_INPUT_BITS_SIZES];
static mb_uint16_t tab_input_regs[MB_TAB_INPUT_REGS_SIZES];
static mb_uint16_t tab_regs[MB_TAB_REGS_SIZES];
static modbus_mapping_t regs_map = 
{
    .nb_bits  = MB_TAB_BITS_SIZES,
    .start_bits = 0,
    .tab_bits = tab_bits_array,
    
	.nb_input_bits = MB_TAB_INPUT_BITS_SIZES,
    .start_input_bits = 0,
    .tab_input_bits = tab_input_bits_array,
    
	.nb_input_registers = MB_TAB_INPUT_REGS_SIZES,
    .start_input_registers = 0,
    .tab_input_registers = tab_input_regs,
    
    .nb_registers = MB_TAB_REGS_SIZES,
    .start_registers = 0,
    .tab_registers = tab_regs,
};

/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);

/* Private functions ---------------------------------------------------------*/
void mb_rtu_slave(void *arg)
{
    modbus_t *mb;
    mb_uint8_t slaveid;
    mb_uint8_t *frame;
    mb_uint16_t length;
    mbport_sercfg_t mb_sercfg;

    /* Create a Modbus object in RTU mode, the connection port is serial port 3. */
    mb = modbus_create(MODBUS_MODE_RTU, mbport_serial_create(3));
    if (mb == MB_NULL)
        goto __exit;
    /* Configure port :
        baud rate - 9600bit/s
        data bits - 8-bit
        parity    - None
        stop bits - 1-bit
        cts       - disable
        rts       - disable
        ch_it     - 2 ms
    */
    MBPORT_SER_CFG(&mb_sercfg, 9600, 8, 0, 1, 0, 0, 2);
    if (modbus_control(mb, MODBUS_CTRL_WRITE, &mb_sercfg) != MB_ENOERR)
    {
        modbus_destroy(mb);
        goto __exit;
    }
    
    /* Set slave identifier */
    modbus_set_slave(mb, 1);
    /* Set registers mapping */
    modbus_set_mapping(mb, &regs_map);
    
    /* Open the connection */
    while (modbus_connect(mb) != MB_ENOERR) {};
    
    /* Poll */
	while (1) 
	{   
        if (modbus_recv(mb, &slaveid, &frame, &length) == MB_ENOERR)
        {
            modbus_replay(mb, slaveid, frame, length);
        }
	}
__exit:
    return;
}

void mb_tcp_slave(void *arg)
{
    modbus_t *mb;
    mb_uint8_t slaveid;
    mb_uint8_t *frame;
    mb_uint16_t length;
    mbport_netcfg_t mb_netcfg;
    
    /* Create a Modbus object in TCP mode. */
    mb = modbus_create(MODBUS_MODE_NONE, mbport_network_create(MBPORT_NET_SERVER, MBPORT_NET_TCP));
    if (mb == MB_NULL)
        goto __exit;
    /* Configure port :
        local ip        - 192.168.137.254
        remote ip       - MB_NULL ( Not used )
        gateway         - 192.168.137.1
        net mask        - 255.255.255.0
        local port      - 502
        remote port     - 0 ( Not used )
        connect timeout - 3000 ms
    */
    MBPORT_NET_CFG(&mb_netcfg, "192.168.137.254", MB_NULL,
        "192.168.137.1", "255.255.255.0", 502, 0, 3000);
    if (modbus_control(mb, MODBUS_CTRL_WRITE, &mb_netcfg) != MB_ENOERR)
    {
        modbus_destroy(mb);
        goto __exit;
    }
    
    /* Set slave identifier */
    modbus_set_slave(mb, 1);
    /* Set registers mapping */
    modbus_set_mapping(mb, &regs_map);
    
    /* Open the connection */
    while (modbus_connect(mb) != MB_ENOERR) {};
    
    /* Poll */
	while (1) 
	{   
        if (modbus_recv(mb, &slaveid, &frame, &length) == MB_ENOERR)
        {
            modbus_replay(mb, slaveid, frame, length);
        }
	}
__exit:
    return;
}

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{

  /* STM32F4xx HAL library initialization:
       - Configure the Flash prefetch, Flash preread and Buffer caches
       - Systick timer is configured by default as source of time base, but user 
             can eventually implement his proper time base source (a general purpose 
             timer for example or other time source), keeping in mind that Time base 
             duration should be kept 1ms since PPP_TIMEOUT_VALUEs are defined and 
             handled in milliseconds basis.
       - Low Level Initialization
     */
  HAL_Init();

  /* Configure the system clock to 168 MHz */
  SystemClock_Config();
  SystemCoreClockUpdate();

#ifdef RTE_CMSIS_RTOS2
  /* Initialize CMSIS-RTOS2 */
  osKernelInitialize ();

  /* Create thread functions that start executing */
  osThreadNew(mb_rtu_slave, NULL, NULL);
  osThreadNew(mb_tcp_slave, NULL, NULL);
  
  /* Start thread execution */
  osKernelStart();
#endif

  /* Infinite loop */
  while (1)
  {
  }
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 168000000
  *            HCLK(Hz)                       = 168000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 8000000
  *            PLL_M                          = 25
  *            PLL_N                          = 336
  *            PLL_P                          = 2
  *            PLL_Q                          = 7
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 5
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;
	
  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();
  
  /* The voltage scaling allows optimizing the power consumption when the device is 
     clocked below the maximum system frequency, to update the voltage scaling value 
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;

  if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    /* Initialization Error */
    _Error_Handler(__FILE__, __LINE__);
  }
  
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;  
  if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    /* Initialization Error */
    _Error_Handler(__FILE__, __LINE__);
  }

	PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
  PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }
  /* STM32F405x/407x/415x/417x Revision Z devices: prefetch is supported  */
  if (HAL_GetREVID() == 0x1001)
  {
    /* Enable the Flash prefetch */
    __HAL_FLASH_PREFETCH_BUFFER_ENABLE();
  }
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */ 

/**
  * @}
  */ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
