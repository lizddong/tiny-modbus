//<<< Use Configuration Wizard in Context Menu >>>
#ifndef __MB_CONFIG_H_
#define __MB_CONFIG_H_

#ifdef __cplusplus
extern "C" {
#endif

/*! \defgroup modbus_cfg Modbus Configuration
 *
 * Most modules in the protocol stack are completly optional and can be
 * excluded. This is specially important if target resources are very small
 * and program memory space should be saved.<br>
 *
 * All of these settings are available in the file <code>modbus_cfg.h</code>
 */
/*! \addtogroup modbus_cfg
 *  @{
 */

// <h>Modbus Global Configuration
// <i>Define modbus parameters

//   <o>Number of modbus object <1-6>
//   <i>Define maximum number of modbus object that can be created at the same time.
//   <i>Default: 4
#define MODBUS_OBJECT_NUM                   2
//   <q>Modbus RTU mode
//   <i>Enable modbus RTU mode. 
#define MODBUS_RTU_ENABLED                  1	
//   <q>Modbus ASCII mode
//   <i>Enable modbus ASCII mode. 
#define MODBUS_ASCII_ENABLED                1
//   <q>Modbus TCP/UDP mode
//   <i>Enable modbus TCP/UDP mode. 
#define MODBUS_TCPUDP_ENABLED               1
//   <q>Modbus uses the debugger
//   <i>Enable debug message.
#define MODBUS_USE_DEBUG                    0

// </h>

// <h>Connection Configuration
// <i>Define modbus connection mode

// <q>Modbus Serial connection
// <i>Enable modbus Serial connection
#define MODBUS_SER_ENABLED              	1

// <q>Modbus Ethernet connection
// <i>Enable modbus ethernet connection
#define MODBUS_ETH_ENABLED              	1


#if ((MODBUS_SER_ENABLED == 0) && (MODBUS_ETH_ENABLED == 0))
    #error "Please select at least one connection mode!"
#endif

// </h>


#ifdef __cplusplus
}
#endif

#endif

//<<< end of configuration section >>>

