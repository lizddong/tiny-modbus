# TinyModbus

TinyModbus is a Modbus software library, It can send and receive data with Modbus protocol compliant devices and can be connected using serial ports or Ethernet, Suitable for MCU, Linux, Win32 platform.

## Features

1. Support Modbus host and slave program design.
2. Supports RTU, ASCII, TCP, UDP, RTU/ASCII Over TCP, and RTC/ASCII Over UDP modes.
3. Multiple Modbus instances can be created at the same time and are independent of each other, which is suitable for multi-port and multi-task scenarios.
4. Modbus core functions and data port separation, code written in C language, can achieve multiple platform transplantation.

## Requirement

TinyModbus is portable and can run on different platforms with the following requirements:

* The data type of the target platform must be adjusted in modbus_priv.h.
* According to TinyModbus modbus_port_t structure type to prepare the target communication port program.  

## Usage

For the complete code, refer to the sample projects in the tests folder, which correspond to the following development environment:

* stm32 - MDK V5.30
* win32 - Code::Blocks 20.03

## Directory
```
/TinyModbus
├── docs                   # Modbus protocol documents  
├── inc                    # Modbus protocol code header file
├── src                    # Modbus protocol code source file    
├── tests                  # Modbus protocol code example project
├── tools                  # Modbus debugger
```
## Tool

* ModbusPoll
* ModbusSlave
