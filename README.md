# TinyModbus

TinyModbus 是一个Modbus软件库，它可以与遵循 Modbus 协议的设备进行数据收发，可以使用串口或以太网连接，适用于MCU、Linux、Win32多种平台。

## 特性

1. 支持 Modbus 主机端和从机端的程序设计。
2. 支持 RTU、ASCII、TCP、UDP、RTU/ASCII Over TCP、RTC/ASCII Over UDP 多种模式。
3. 可同时创建多个Modbus实例，彼此相互独立，适用于多端口多任务的应用场景。
4. Modbus核心功能和数据端口分离，代码采用C语言编写，可以实现多个平台移植。

## 必要条件

TinyModbus 是可移植的，可以运行在不同平台上，只需要满足以下要求:

* 目标平台的数据类型必须在modbus_priv.h中进行调整。
* 根据TinyModbus的modbus_port_t结构类型编写目标通讯端口程序。

## 用法

完整的代码可以参考tests文件夹中的示例工程，各个工程对应开发环境如下：

* stm32 - MDK V5.30
* win32 - Code::Blocks 20.03

## 目录
```
/TinyModbus
├── docs                   # Modbus协议文档
├── inc                    # Modbus协议代码头文件
├── src                    # Modbus协议代码源文件
├── tests                  # Modbus协议代码的示例工程
├── tools                  # Modbus调试工具
```
## 工具

* ModbusPoll
* ModbusSlave
